const $ = require('jquery');
const storage = require('./storage');
const socket = require('./socket');
const events = require('./events');

const KEY_SPACE = 32;

let $el;
const ids = ['daily', 'weekly', 'monthly', 'all'];
const gameTypeParams = require('./constants/gameTypeParams');

const resultTableTemplate = require('../templates/resultTable.handlebars');
const tabPaneTemplate = require('../templates/tabPane.handlebars');
const resultTooltipTemplate = require('../templates/resultTooltip.handlebars');
const resultDetailTemplate = require('../templates/resultDetail.handlebars');

let $resultDialog;
const GamePlayer = require('./game/player');
let player;

module.exports = {
    init: init
};

function init() {
    $el = $('#results');

    socket.on(events.GAME_RESULTS, (data) => {
        updateTables(data.results);
    });

    $el.find('.results-period a').on('click', function (e) {
        e.preventDefault();
        storage.set('tab', $(this).attr('href'));
    });

    $resultDialog = $('#result-dialog');
    $resultDialog.on('hidden.bs.modal', () => {
        if (player) {
            player.stop();
            player = null;
        }
        setResultDialogContent('<div class="loader"></div>');
    });
    $resultDialog.on('keyup', (e) => {
        if (e.which === KEY_SPACE) {
            e.preventDefault();
            if (player) {
                player.toggle();
            }
        }
    });
    $resultDialog.on('focus', 'button', (e) => {
        e.preventDefault();
        $resultDialog.focus();
    });

    $(document.body).on('click', '.js-show-result', function (e) {
        e.preventDefault();
        $(this).popover('hide');
        showResultDetails(this.dataset.resultId);
    });
}

function updateTables(data) {
    let html = '';
    let results;
    let groups;
    let tables;

    let now = new Date();
    let date;

    for (let i = 0, ii = data.length; i < ii; i++) {
        results = data[i];
        // группируем результаты по типу
        groups = {
            1: [],	// новичок
            2: [],	// любитель
            3: []	// профессионал
        };
        $.each(results, (index, value) => {
            // определяем новые результаты
            date = new Date(value.date);
            if ((now.getTime() - date.getTime()) / 1000 <= 5) {
                value.isNew = true;
            }

            groups[value.type].push(value);
        });
        tables = '';
        $.each(groups, (index, values) => {
            tables += resultTableTemplate({
                type: index,
                rows: values
            });
        });

        html += tabPaneTemplate({
            id: ids[i],
            content: tables
        });
    }

    $el.find('tr').popover('destroy');

    $el.find('.results-tables').html(html);
    $el
        .find('.results-period li').removeClass('active')
        .find('a[href="' + storage.get('tab', '#results-daily') + '"]').tab('show');

    applyPopover();
}

function applyPopover() {
    $el.find('tr').popover({
        animation: false,
        container: 'body',
        content: function () {
            if (!this.hasAttribute('data-result')) {
                return;
            }
            let result = JSON.parse(this.getAttribute('data-result'));

            return resultTooltipTemplate(result);
        },
        title: '',
        html: true,
        trigger: 'hover',
        template: '<div class="popover popover-result" role="tooltip"><div class="arrow"></div><div class="popover-content"></div></div>'
    });
}

// показывает модальное окно с информацией о результате игры
function showResultDetails(resultId) {
    $resultDialog.modal('show');

    $.ajax({
        url: '/api/results/' + resultId,
        dataType: 'json',
        method: 'GET',
        success: (data) => {
            data.hor = gameTypeParams[data.type][0];
            data.vert = gameTypeParams[data.type][1];
            data.minesCount = gameTypeParams[data.type][2];

            setResultDialogContent(resultDetailTemplate(data));

            const canvas = document.getElementById('player-game-area');
            const flagCounter = document.getElementById('player-flags-count');
            const timeCounter = document.getElementById('player-time-count');
            const statusBar = document.getElementById('player-status');
            const $click = $('#player-click');

            player = new GamePlayer(
                data.hor, data.vert, data.minesCount, data.mines, data.actions, data.result, canvas, flagCounter, timeCounter, statusBar, $click
            );
            player.showAll();

            const $btnStart = $('#player-start');
            const $btnStop = $('#player-stop');
            const $btnPause = $('#player-pause');
            const $btnContinue = $('#player-continue');

            player.on('start', () => {
                $btnStart.hide();
                $btnStop.show();
                $btnPause.show();
                $btnContinue.hide();
            });
            player.on('stop', () => {
                $btnStart.show();
                $btnStop.hide();
                $btnPause.hide();
                $btnContinue.hide();
            });
            player.on('pause', () => {
                $btnStart.hide();
                $btnStop.show();
                $btnPause.hide();
                $btnContinue.show();
            });
            player.on('continue', () => {
                $btnStart.hide();
                $btnStop.show();
                $btnPause.show();
                $btnContinue.hide();
            });

            $btnStart.on('click', () => {
                player.start();
            });
            $btnStop.on('click', () => {
                player.stop();
            });
            $btnPause.on('click', () => {
                player.pause();
            });
            $btnContinue.on('click', () => {
                player.continue();
            });
        },
        error: () => {
            setResultDialogContent('<div class="modal-body"><div class="alert alert-danger" style="margin: 0;">Не удалось загрузить игру</div></div>');
        }
    });
}

function setResultDialogContent(html) {
    $resultDialog.find('.modal-content').html(html);
    $resultDialog.modal('handleUpdate');
}
