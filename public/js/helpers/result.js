module.exports = function (value, withMs = true) {
    let totalSeconds = Math.floor(value / 100);

    let minutes = align(Math.floor(totalSeconds / 60));
    let seconds = align(totalSeconds % 60);

    let result = minutes + ':' + seconds;

    if (withMs) {
        result += '.' + align(value % 100);
    }

    return result;
};

function align(value) {
    return value < 10 ? '0' + value : value;
}