module.exports = function (value, result) {
    let seconds = result / 100;
    return (value / seconds).toFixed(2);
};