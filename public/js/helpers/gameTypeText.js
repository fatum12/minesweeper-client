const gameTypeName = require('../constants/gameTypeName');

module.exports = function (value) {
    return gameTypeName[value];
};