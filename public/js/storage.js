const config = require('./config');

let storage = {};

if (isStorageAvailable()) {
    storage.get = function (key, defaultValue) {
        defaultValue = defaultValue || null;
        key = config.namespace + '_' + key;

        if (localStorage.getItem(key) !== null) {
            return localStorage.getItem(key);
        }

        return defaultValue;
    };

    storage.set = function (key, value) {
        key = config.namespace + '_' + key;
        localStorage.setItem(key, value);
    };
} else {
    storage.get = function (key, defaultValue) { return defaultValue; };
    storage.set = function () {};
}

function isStorageAvailable() {
    try {
        return 'localStorage' in window && window['localStorage'] !== null;
    } catch (e) {
        return false;
    }
}

module.exports = storage;