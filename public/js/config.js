const config = {
    debug: true,
    namespace: 'minesweeper',
    serverUrl: '/minesweeper'
};

// настройки для production
if (window.location.hostname === 'minesweeper.odd.su') {
    config.debug = false;
}

module.exports = config;