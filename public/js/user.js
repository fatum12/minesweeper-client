const socket = require('./socket');
const nameDialog = require('./dialogs/name');
const storage = require('./storage');
const events = require('./events');

let guest = true;
let name = null;
let id;
let dialogProcessed = false;

function init() {
    socket
        .on(events.USER_AUTH, (data) => {
            id = data.userId;
        })
        .on(events.CONNECT, () => {
            if (dialogProcessed) {
                socket.send(events.GAME_JOIN, {
                    name: name
                });
            }
        });

    nameDialog.init()
        .then(
            (userName) => {
                // обычный режим
                name = userName;
                socket.send(events.GAME_JOIN, {
                    name: userName
                });
                guest = false;
                storage.set('username', userName);
                dialogProcessed = true;
            },
            () => {
                // анонимный режим
                socket.send(events.GAME_JOIN);
                dialogProcessed = true;
            }
        );
}

function isGuest() {
    return guest;
}

function getId() {
    return id;
}

module.exports = {
    init: init,
    isGuest: isGuest,
    getId: getId
};
