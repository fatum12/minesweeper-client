module.exports = {
    /**
     * Подписка на событие
     * Использование:
     *  menu.on('select', function(item) { ... }
     */
    on(eventName, handler) {
        if (!this._eventHandlers) this._eventHandlers = {};
        if (!this._eventHandlers[eventName]) {
            this._eventHandlers[eventName] = [];
        }
        this._eventHandlers[eventName].push(handler);

        return this;
    },

    /**
     * Прекращение подписки
     *  menu.off('select',  handler)
     */
    off(eventName, handler) {
        let handlers = this._eventHandlers && this._eventHandlers[eventName];
        if (!handlers) return;
        for (let i = 0; i < handlers.length; i++) {
            if (handlers[i] == handler) {
                handlers.splice(i--, 1);
            }
        }

        return this;
    },

    once(eventName, handler) {
        let self = this;
        let callback = function () {
            self.off(eventName, callback);
            handler.apply(self, [].slice.call(arguments, 0));
        };

        self.on(eventName, callback);

        return self;
    },

    /**
     * Генерация события с передачей параметров
     *  this.trigger('select', item);
     */
    trigger(eventName /*, ... */) {

        if (!this._eventHandlers || !this._eventHandlers[eventName]) {
            return; // обработчиков для события нет
        }

        // вызвать обработчики
        let handlers = this._eventHandlers[eventName];
        for (let i = 0; i < handlers.length; i++) {
            handlers[i].apply(this, [].slice.call(arguments, 1));
        }

        return this;
    }
};