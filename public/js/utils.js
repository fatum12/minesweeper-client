const config = require('./config');

let utils = {
    getRandom(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },
    disableInput($input) {
        $input
            .addClass('disabled')
            .prop('disabled', true);
    },
    enableInput($input) {
        $input
            .removeClass('disabled')
            .prop('disabled', false);
    }
};

if (
    config.debug &&
    typeof console !== "undefined" &&
    typeof console.log !== "undefined" &&
    typeof Function.prototype.bind !== "undefined"
) {
    utils.log = console.log.bind(console);
} else {
    utils.log = function () {};
}

module.exports = utils;