const $ = require('jquery');
const SockJS = require('sockjs-client');

const eventable = require('./mixins/eventable');
const config = require('./config');
const utils = require('./utils');
const events = require('./events');

let ws;
let timer;
let defaultTimeout = 5000;
let tries = 0;
let connected = false;

let socket = {
    connect: connect,
    send: send,
    isConnected: () => { return connected; }
};

$.extend(socket, eventable);

socket.on(events.RELOAD, () => {
    window.location.reload();
});

function connect() {
    ws = new SockJS(config.serverUrl);

    ws.onopen = function () {
        utils.log('Соединение установлено');
        
        clearTimeout(timer);
        tries = 0;
        connected = true;
        socket.trigger(events.CONNECT);
    };
    
    ws.onclose = function (event) {
        utils.log('Соединение закрыто. Код: ' + event.code + ', причина: ' + event.reason);

        clearTimeout(timer);
        connected = false;
        tries++;
        if (tries > 6) {
            tries = 1;
        }
        timer = setTimeout(() => {
            connect();
        }, defaultTimeout * tries);

        socket.trigger(events.DISCONNECT, {
            date: new Date()
        });
    };
    
    ws.onmessage = function (event) {
        let data = JSON.parse(event.data);
        utils.log('Получено:', data);

        socket.trigger(data.id, data);
    };
}

function send(event, data = {}) {
    data.id = event;
    ws.send(JSON.stringify(data));
    utils.log('Отправлено:', data);
}

module.exports = socket;
