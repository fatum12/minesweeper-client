const $ = require('jquery');

const mainGame = require('../game/index');

let $dialog;
let $form;

function init() {
    $dialog = $('#settings-dialog');
    $form = $dialog.find('form');

    $('#custom-game-type').on('click', (e) => {
        e.preventDefault();
        showDialog();
    });

    $form.on('submit', (e) => {
        e.preventDefault();
        mainGame.newGame($form.find('[name="hor"]').val(), $form.find('[name="vert"]').val(), $form.find('[name="mines"]').val());
        hideDialog();
    });

    $dialog.on('shown.bs.modal', () => {
        $form.find('input:first:visible').focus();
    });
}

function initForm() {
    $form
        .find('[name="hor"]').val(mainGame.getGame().nHor)
        .end().find('[name="vert"]').val(mainGame.getGame().nVert)
        .end().find('[name="mines"]').val(mainGame.getGame().nMines);
}

function showDialog() {
    initForm();
    $dialog.modal('show');
}

function hideDialog() {
    $dialog.modal('hide');
}

module.exports = {
    init: init
};