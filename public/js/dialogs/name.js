const $ = require('jquery');

const storage = require('../storage');

let $dialog;
let $form;
let $input;

function init() {
    $dialog = $('#name-dialog');
    $form = $dialog.find('form');
    $input = $form.find('.name-dialog-input');

    let deferred = $.Deferred();

    $dialog.on('shown.bs.modal', () => {
        $form.find('input:first:visible').focus();
    });

    $form.on('submit', (e) => {
        e.preventDefault();
        if (validate()) {
            deferred.resolve($.trim($input.val()));
            hideDialog();
        }
    });

    $dialog.find('.name-dialog-anonymous').on('click', (e) => {
        e.preventDefault();
        deferred.reject();
        hideDialog();
    });

    $input.on('keyup', () => {
        validate();
    });

    showDialog();

    return deferred.promise();
}

function initForm() {
    $input.val(storage.get('username', ''));
}

function showDialog() {
    initForm();
    resetError();
    $dialog.modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
}

function hideDialog() {
    $dialog.modal('hide');
}

function validate() {
    let name = $.trim($input.val());

    if (name === '') {
        setError('Введите ваше имя');
        return false;
    } else if (name.length < 3) {
        setError('Имя должно содержать не менее 3 символов');
        return false;
    } else {
        resetError();
        return true;
    }
}

function setError(text) {
    $input.focus()
        .closest('.form-group').addClass('has-error')
        .find('.help-block').text(text).show();
}

function resetError() {
    $input.closest('.form-group').removeClass('has-error')
        .find('.help-block').text('').hide();
}

module.exports = {
    init: init
};