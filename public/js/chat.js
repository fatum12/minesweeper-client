const $ = require('jquery');
require('jquery-ui/themes/base/core.css');
require('jquery-ui/themes/base/resizable.css');
require('jquery-ui/ui/core');
require('jquery-ui/ui/widgets/resizable');

const socket = require('./socket');
const sound = require('./sound');
const user = require('./user');
const storage = require('./storage');
const utils = require('./utils');
const events = require('./events');

let $el;
let $form;
let $input;
let $btn;
let $messages;
let $users;

const usersListTemplate = require('../templates/usersList.handlebars');
let disconnectShown = false;

function init() {
    $el = $('#chat');
    $form = $el.find('form');
    $input = $el.find('.chat-input');
    $btn = $el.find('.chat-send');
    $messages = $el.find('.chat-messages');
    $users = $el.find('.chat-users');

    socket
        .on(events.CONNECT, () => {
            clearChat();
            utils.enableInput($input);
            checkBtnState();
            disconnectShown = false;
        })
        .on(events.DISCONNECT, (data) => {
            utils.disableInput($input);
            utils.disableInput($btn);

            if (!disconnectShown) {
                disconnectShown = true;
                addMessage('disconnect', data);
            }
        })
        .on(events.CHAT_MESSAGE, (data) => {
            addMessage(data.template, data.data);
        })
        .on(events.GAME_USERS, (data) => {
            updateUsersList(data.users);
        })
        .on(events.CHAT_HISTORY, (data) => {
            if (data.clear) {
                clearChat();
            }
            if (data.messages.length) {
                let html = '';
                for (let i = data.messages.length - 1; i >= 0; i--) {
                    html += buildMessage('history', data.messages[i]);
                }
                $(html).hide().prependTo($messages).slideDown('fast');
            }
        });

    $input
        .on('keyup', (e) => {
            checkBtnState();
        })
        .on('keypress', (e) => {
            // отправка сообщений чата по enter
            if (e.which == 0xA || e.which == 0xD) {
                e.preventDefault();
                $form.trigger('submit');
            }
        });

    $form.on('submit', (e) => {
        e.preventDefault();
        sendMessage($input.val());
        $input.val('').focus();
        utils.disableInput($btn);
    });

    restoreSize();
    resizable();
    autoClean();
}

function addMessage(template, data) {
    $messages.append(buildMessage(template, data));
    scrollToBottom();
    if (template == 'default') {
        sound.play('message');
    }
}

function buildMessage(templateName, data) {
    data = data || {};
    let template = require(`../templates/messages/message.${templateName}.handlebars`);
    return template(data);
}

function scrollToBottom() {
    $messages
        .stop()
        .animate({scrollTop: $messages.prop('scrollHeight')}, 600);
}

function sendMessage(text) {
    text = $.trim(text);
    if (text === '') {
        return;
    }
    socket.send(events.CHAT_MESSAGE, {
        message: text
    });
}

function checkBtnState() {
    if ($.trim($input.val()) !== '') {
        utils.enableInput($btn);
    } else {
        utils.disableInput($btn);
    }
}

function updateUsersList(users) {
    let currentUserId = user.getId();

    $.each(users, (index, user) => {
        if (user.id == currentUserId) {
            users[index].current = true;
        }
    });

    // сортируем массив так, чтобы анонимные игроки были внизу списка
    users.sort((a, b) => {
        if (a.guest < b.guest) {
            return -1;
        }
        if (a.guest > b.guest) {
            return 1;
        }
        return 0;
    });

    $users.html(usersListTemplate({
        users: users
    }));
}

function resizable() {
    $el.resizable({
        minWidth: 400,
        maxWidth: 1100,
        minHeight: 200,
        maxHeight: 600,
        stop: function (event, ui) {
            storage.set('chat_width', ui.size.width);
            storage.set('chat_height', ui.size.height);
        }
    });
}

function restoreSize() {
    if (storage.get('chat_width') && storage.get('chat_height')) {
        $el
            .width(storage.get('chat_width'))
            .height(storage.get('chat_height'));
    }
}

function clearChat() {
    $messages.empty();
}

// удаляет старые сообщения чата каждые 30 минут
function autoClean() {
    const LIMIT = 30;

    setInterval(() => {
        let chatLength = $messages.find('> .chat-message').length;

        if (chatLength > LIMIT) {
            $messages.find('> .chat-message:lt(' + (chatLength - LIMIT) + ')').remove();
        }
    }, 30 * 60 * 1000);
}

module.exports = {
    init: init
};
