require('bootstrap/dist/css/bootstrap.css');
require('bootstrap/dist/css/bootstrap-theme.css');

require('../css/fonts.css');
require('../css/base.css');
require('../css/layout.css');
require('../css/animations.css');
require('../css/app.css');

const $ = require('jquery');

const moment = require('moment');
moment.locale('ru');

require('bootstrap/dist/js/bootstrap');

const user = require('./user');
const sound = require('./sound');
const game = require('./game/index');
const settingsDialog = require('./dialogs/settings');
const results = require('./results');
const chat = require('./chat');
const socket = require('./socket');
const watch = require('./game/watch');

$(document).ready(() => {
    user.init();
    sound.init();
    settingsDialog.init();
    results.init();
    chat.init();
    game.init();
    watch.init();
    socket.connect();
});
