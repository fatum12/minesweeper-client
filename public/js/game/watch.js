const $ = require("jquery");
const socket = require("../socket");
const events = require("../events");
const Game = require('./game');
const View = require("./view");
const gameStatus = require('../constants/gameStatus');

function init() {
    const game = new Game();

    const canvas = document.getElementById('watch-game-area');
    const flagCounter = document.getElementById('watch-flags-count');
    const timeCounter = document.getElementById('watch-time-count');
    const statusBar = document.getElementById('watch-status');
    const $click = $('#watch-click');

    const view = new View(game, canvas, flagCounter, timeCounter, statusBar);

    const $watchDialog = $('#watch-dialog');
    $watchDialog.on('hide.bs.modal', () => {
        view.stopTimer();
        socket.send(events.WATCH_STOP);
    });

    socket.on(events.WATCH_INIT, data => {
        $watchDialog.modal('show');

        $watchDialog.find('.watch-player-name').text(data.player.name);
        $watchDialog.find('.watch-player-status')
            .addClass('label-success')
            .removeClass('label-danger')
            .text('online')
        ;
        $click.hide();

        game.newGameFromState(data.game);
        view.setStatus('Наблюдение');
        view.paintAll();
        view.updateTimer();
        if (data.game.status === gameStatus.PLAY) {
            view.startTimer();
        }
        $watchDialog.modal('handleUpdate');
    });
    socket.on(events.WATCH_ACTION, data => {
        game[data.action](data.r, data.c);

        const x = (data.c - 1) * view.cellWidth;
        const y = (data.r - 1) * view.cellHeight;

        $click
            .css({
                left: x,
                top: y
            })
            .show()
        ;
    });
    socket.on(events.WATCH_LEAVE, () => {
        view.stopTimer();
        $watchDialog.find('.watch-player-status')
            .removeClass('label-success')
            .addClass('label-danger')
            .text('offline')
        ;
    });
    socket.on(events.DISCONNECT, () => {
        $watchDialog.modal('hide');
    });

    $(document.body).on('click', '.js-watch-player', function (e) {
        e.preventDefault();

        socket.send(events.WATCH_START, {
            pid: this.dataset.userId
        });
    });
}

module.exports = {
    init: init,
};
