const $ = require('jquery');
const utils = require('../utils');
const Field = require('./Field');
const eventable = require('../mixins/eventable');
const localStrategy = require('./strategies/local');

const gameType = require('../constants/gameType');
const gameStatus = require('../constants/gameStatus');
const gameTypeParams = require('../constants/gameTypeParams');

class Game {

    constructor() {
        this.nHor = 9;
        this.nVert = 9;
        this.nMines = 10;
        this.nOpen = 0;
        this.nFlags = 0;
        this.status = gameStatus.READY;
        this.firstAction = true;
        this.cells = new Field();
        this.startTime = null;
        this.endTime = null;
        this.type = gameType.BEGINNER;
        this.strategy = localStrategy;
    }

    static determineGameType(hor, vert, mines) {
        let found = null;
        let params;

        for (let type in gameTypeParams) {
            params = gameTypeParams[type];
            if (params[0] == hor && params[1] == vert && params[2] == mines) {
                found = type;
                break;
            }
        }

        return found ? parseInt(found) : gameType.CUSTOM;
    }

    newGame(hor, vert, mines, minesState) {
        hor = parseInt(hor) || 0;
        if (hor > 0) {
            if (hor < 6) {
                hor = 6;
            }
            this.nHor = hor;
        }

        vert = parseInt(vert) || 0;
        if (vert > 0) {
            if (vert < 6) {
                vert = 6;
            }
            this.nVert = vert;
        }

        mines = parseInt(mines) || 0;
        if (mines > 0) {
            if (mines < 3) {
                mines = 3;
            }
            if (mines > this.nVert * this.nHor * 0.8) {
                mines = Math.floor(this.nVert * this.nHor * 0.8);
            }
            this.nMines = mines;
        }

        this.type = this.constructor.determineGameType(this.nHor, this.nVert, this.nMines);

        this.nOpen = 0;
        this.nFlags = 0;
        this.setFlagCount(0);
        this.firstAction = true;
        this.startTime = null;
        this.endTime = null;

        this.cells.setSize(this.nVert, this.nHor);

        this.status = gameStatus.PLAY;

        this.trigger('init', this.type, this.nHor, this.nVert, this.nMines);

        if (minesState) {
            minesState.forEach((position) => {
                this.cells.makeMine(position[0], position[1]);
            });
            this.trigger('newGame', this.type, this.nHor, this.nVert, this.nMines);
        }
    }

    newGameByGameType(type) {
        if (type in gameTypeParams) {
            this.newGame(...gameTypeParams[type]);
        } else {
            this.newGame();
        }
    }

    /**
     * @param {Object} state
     */
    newGameFromState(state) {
        const params = gameTypeParams[state.type];
        if (!params) {
            throw new Error('game from state: unknown game type');
        }
        this.nHor = params[0];
        this.nVert = params[1];
        this.nMines = params[2];
        this.type = state.type;

        this.nOpen = state.openCount;
        this.nFlags = 0;
        this.setFlagCount(state.flagsCount);
        this.firstAction = state.firstAction;
        this.startTime = state.startTime ? new Date(state.startTime) : null;
        this.endTime = state.endTime ? new Date(state.endTime) : null;

        if (state.cells.length) {
            this.cells.cells = state.cells;
        } else {
            this.cells.setSize(this.nVert, this.nHor);
        }

        this.status = state.status;
    }

    // заполняет поле случайными минами
    createMines() {
        this.strategy.create(this, this.type, this.nHor, this.nVert, this.nMines);
    }

    endGame() {
        this.endTime = new Date();
        this.trigger('endGame', this.status);

        // делаем все мины видимыми
        for (let col = 1; col <= this.nHor; col++) {
            for (let row = 1; row <= this.nVert; row++) {
                if (this.cells.isMine(row, col)) {
                    this.cells.makeOpen(row, col);
                    this.trigger('update', row, col);
                }
            }
        }

        this.status = gameStatus.READY;
    }

    // открывает ячейку
    open(row, col) {
        if (!this.canAct(row, col)) {
            return;
        }
        this.strategy.open(row, col);

        if (this.firstAction) {
            // если это первый клик
            if (this.cells.isMine(row, col)) {
                // и попали на мину - переносим мину
                utils.log('Первая мина');
                this.moveMine(row, col);
            }
            // запускаем таймер
            this.startTime = new Date();
            this.firstAction = false;
            this.trigger('gameStarted');
        }
        if (this.cells.isOpen(row, col) || this.cells.isFlag(row, col)) {
            return;
        }
        if (this.cells.isMine(row, col)) {
            // игрок подорвался
            this.cells.makeFail(row, col);
            this.status = gameStatus.LOSE;
            this.endGame();
        } else {
            this.openCells(row, col);
            this.trigger('open');

            if (this.isWin()) {
                this.status = gameStatus.WIN;
                this.endGame();
            }
        }
    }

    // ставит флаг, вопрос
    mark(row, col) {
        if (!this.canAct(row, col)) {
            return;
        }
        this.strategy.mark(row, col);

        if (this.cells.isOpen(row, col)) {
            return;
        }

        if (this.cells.isFlag(row, col)) {
            this.cells.makeQuestion(row, col);    // сделать вопросом
            this.cells.resetFlag(row, col);       // сбросить флаг
            this.setFlagCount(-1);
        }
        else if (this.cells.isQuestion(row, col)) {
            this.cells.resetQuestion(row, col);   // сбросить вопрос
        }
        else {
            this.cells.makeFlag(row, col);        // сделать флагом
            this.setFlagCount(1);
        }
        this.trigger('update', row, col);
    }

    // открывает ячейки вокруг заданной
    openAround(row, col) {
        if (!this.canAct(row, col)) {
            return;
        }
        this.strategy.openAround(row, col);

        let number = this.cells.getNumber(row, col);
        if (this.cells.isOpen(row, col) && number > 0) {
            let flags = 0;
            let i, j;

            for (i = row - 1; i <= row + 1; i++) {
                for (j = col - 1; j <= col + 1; j++) {
                    if (i === row && j === col) {
                        continue;
                    }
                    if (!this.cells.isOpen(i, j) && this.cells.isFlag(i, j)) {
                        flags++;
                    }
                }
            }

            if (flags === number) {
                for (i = row - 1; i <= row + 1; i++) {
                    for (j = col - 1; j <= col + 1; j++) {
                        if (this.cells.isMine(i, j) && !this.cells.isFlag(i, j)) {
                            // игрок подорвался
                            this.status = gameStatus.LOSE;
                            this.endGame();
                            return;
                        }
                        this.openCells(i, j);
                        this.trigger('open');

                        if (this.isWin()) {
                            this.status = gameStatus.WIN;
                            this.endGame();
                            return;
                        }
                    }
                }
            }
        }
    }

    setFlagCount(value) {
        this.nFlags += value;
        this.trigger('flags', this.nMines - this.nFlags);
    }

    // рекурсивная функция для открытия ячеек
    openCells(r, c) {
        if (!this.isValidCell(r, c)) {
            return;
        }
        if (this.cells.isOpen(r, c) || this.cells.isMine(r, c) || this.cells.isFlag(r, c)) {
            return;
        }
        this.cells.makeOpen(r, c);
        this.nOpen++;
        this.trigger('update', r, c);
        // вокруг ячейки нет мин
        if (this.cells.getNumber(r, c) === 0) {
            this.openCells(r - 1, c - 1);
            this.openCells(r - 1, c);
            this.openCells(r - 1, c + 1);
            this.openCells(r, c - 1);
            this.openCells(r, c + 1);
            this.openCells(r + 1, c - 1);
            this.openCells(r + 1, c);
            this.openCells(r + 1, c + 1);
        }
    }

    isWin() {
        return this.nOpen === this.getNeedOpenCount();
    }

    isValidCell(row, col) {
        return row >= 1 && col >= 1 && row <= this.nVert && col <= this.nHor;
    }

    canAct(row, col) {
        return this.status === gameStatus.PLAY && this.isValidCell(row, col);
    }

    getOpenedCount() {
        return this.nOpen;
    }

    getNeedOpenCount() {
        return this.nHor * this.nVert - this.nMines;
    }

    getResult() {
        if (!this.startTime) {
            return 0;
        }
        const now = this.endTime ? this.endTime : new Date();
        return Math.floor((now.getTime() - this.startTime.getTime()) / 10);
    }

    sync(cellStates) {
        this.strategy.sync(this, this.type, this.nHor, this.nVert, this.nMines, this.cells, cellStates);
    }

    // Переносит мину в другую позицию
    moveMine(r, c) {
        for (let row = 1; row <= this.nVert; row++) {
            for (let col = 1; col <= this.nHor; col++) {
                if (!this.cells.isMine(row, col)) {
                    this.cells.makeMine(row, col);
                    this.cells.resetMine(r, c);
                    return;
                }
            }
        }
    }
}

$.extend(Game.prototype, eventable);

module.exports = Game;
