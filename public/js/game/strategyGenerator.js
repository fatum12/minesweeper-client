const localStrategy = require('./strategies/local');
const remoteStrategy = require('./strategies/remote');
const socket = require('../socket');
const gameType = require('../constants/gameType');

module.exports = function (type) {
    if (type == gameType.CUSTOM || !socket.isConnected()) {
        return localStrategy;
    } else {
        return remoteStrategy;
    }
};
