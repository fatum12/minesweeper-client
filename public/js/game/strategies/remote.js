const socket = require('../../socket');
const events = require('../../events');

module.exports = {
    create(game, gameType, nHor, nVert, nMines) {
        socket.send(events.GAME_CREATE, {
            type: gameType
        });
    },
    sync(game, gameType, nHor, nVert, nMines, cells, cellStates) {
        game.cells.cells = cellStates;

        game.trigger('newGame', gameType, nHor, nVert, nMines);
    },
    open(row, col) {
        socket.send(events.GAME_OPEN, {
            r: row,
            c: col
        });
    },
    mark(row, col) {
        socket.send(events.GAME_MARK, {
            r: row,
            c: col
        });
    },
    openAround(row, col) {
        socket.send(events.GAME_OPEN_AROUND, {
            r: row,
            c: col
        });
    }
};
