const utils = require('../../utils');

module.exports = {
    create(game, gameType, nHor, nVert, nMines) {
        // заполняем поле минами
        let n = 0;	// количество мин
        let row, col;
        do {
            row = utils.getRandom(1, nVert);
            col = utils.getRandom(1, nHor);
            if (!game.cells.isMine(row, col)) {
                game.cells.makeMine(row, col);
                n++;
            }
        } while (n < nMines);

        game.trigger('newGame', gameType, nHor, nVert, nMines);
    },
    sync(game, gameType, nHor, nVert, nMines, cells, cellStates) {},
    open(row, col) {},
    mark(row, col) {},
    openAround(row, col) {}
};
