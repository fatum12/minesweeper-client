const Game = require('./game');
const View = require('./view');
const eventable = require('../mixins/eventable');
const $ = require('jquery');
const utils = require('../utils');

class Player {

    constructor(hor, vert, minesCount, mines, actions, result, canvas, flagCounter, timeCounter, statusBar, $click) {
        this.hor = hor;
        this.vert = vert;
        this.minesCount = minesCount;
        this.mines = mines;
        this.actions = actions;
        this.result = result;
        this.$click = $click;
        this.timer = null;
        this.currentStep = -1;
        // текущее значение таймера (мс)
        this.currentTimerValue = 0;
        this.paused = true;

        this.game = new Game();
        this.view = new View(this.game, canvas, flagCounter, timeCounter, statusBar);
        this.game.on('endGame', () => {
            this._onEnd();
        });
    }

    // показывает все игровое поле
    showAll() {
        this.game.newGame(this.hor, this.vert, this.minesCount, this.mines);

        for (let row = 1; row <= this.vert; row++) {
            for (let col = 1; col <= this.hor; col++) {
                this.game.cells.makeOpen(row, col);
            }
        }
        this.view.paintAll();
        this.view.setStatus('Повтор игры');
        this.view.setTimer(this.result);
        this.$click.hide();
    }

    start() {
        utils.log('player: start');

        this.stopTimer();
        this.$click.hide();
        this.game.newGame(this.hor, this.vert, this.minesCount, this.mines);
        this.currentStep = -1;
        this.currentTimerValue = 0;
        this.paused = false;

        this.playFromStep(0, 500);

        this.trigger('start');
    }

    playFromStep(step, delay) {
        const stepCount = this.actions.length;
        const self = this;

        function showAction(action) {
            self.game[action[1]](action[2], action[3]);
            self.currentStep = step;

            step++;
            if (step < stepCount) {
                let nextAction = self.actions[step];
                self.timer = setTimeout(() => {
                    showAction(nextAction);
                }, (new Date(nextAction[0])).getTime() - (new Date(action[0])).getTime());
            }

            self.showClick(action[2], action[3]);
        }

        this.timer = setTimeout(() => {
            showAction(this.actions[step]);
        }, delay);
    }

    stop() {
        utils.log('player: stop');

        this._onEnd();
        this.showAll();
    }

    _onEnd() {
        this.stopTimer();
        this.currentStep = -1;
        this.currentTimerValue = 0;
        this.paused = true;
        this.trigger('stop');
    }

    stopTimer() {
        clearTimeout(this.timer);
        this.view.stopTimer();
    }

    pause() {
        utils.log('player: pause');

        this.stopTimer();
        this.currentTimerValue = new Date() - this.game.startTime;
        this.paused = true;

        this.trigger('pause');
    }

    continue() {
        utils.log('player: continue');

        this.paused = false;
        let step = this.currentStep;
        if (step >= 0 && step + 1 < this.actions.length) {
            step++;
            let delay = new Date(this.actions[step][0]).getTime() - new Date(this.actions[0][0]) - this.currentTimerValue;
            delay = Math.max(0, delay);

            this.game.startTime = new Date(new Date().getTime() - this.currentTimerValue);
            this.playFromStep(step, delay);
            this.view.updateTimer();
            this.view.startTimer();

            this.trigger('continue');
        } else {
            this.start();
        }
    }

    toggle() {
        utils.log('player: toggle, paused = ', this.paused);

        if (this.paused) {
            this.continue();
        } else {
            this.pause();
        }
    }

    showClick(row, col) {
        let x = (col - 1) * this.view.cellWidth;
        let y = (row - 1) * this.view.cellHeight;

        this.$click
            .css({
                left: x,
                top: y
            })
            .show();
    }
}

$.extend(Player.prototype, eventable);

module.exports = Player;
