const utils = require('../utils');
const resultFormatter = require('../helpers/result');
const gameStatus = require('../constants/gameStatus');

const w = 20;   // базовая ширина ячейки
const h = 20;   // базовая высота ячейки

const mine = new Image();
const flag = new Image();
const question = new Image();

mine.src = 'images/cells/mine.png';
flag.src = 'images/cells/flag.png';
question.src = 'images/cells/question.png';

const colors = [
    '',
    '0000ff',   // 1
    '008000',   // 2
    'ff0000',   // 3
    '000080',   // 4
    '800000',   // 5
    '008080',   // 6
    '000000',   // 7
    '808080'    // 8
];

class View {

    constructor(game, canvas, flagCounter, timeCounter, statusBar) {
        this.game = game;
        this.canvas = canvas;
        this.ctx = canvas.getContext('2d');
        this.flagCounter = flagCounter;
        this.timeCounter = timeCounter;
        this.statusBar = statusBar;
        this.timer = null;

        // блокируем контекстное меню
        this.canvas.oncontextmenu = () => {
            return false;
        };

        this.game
            .on('init', () => {
                this.stopTimer();
                this.timeCounter.innerHTML = '00:00';
            })
            .on('newGame', (type, nHor, nVert, nMines) => {
                utils.log('New game', nHor, nVert, nMines, 'gameType =', type);

                this.setStatus('Началась новая игра');
                this.paintAll();
            })
            .on('endGame', (status) => {
                utils.log('End game, status =', status);

                this.stopTimer();
                this.updateTimer();

                // проигрыш
                if (status === gameStatus.LOSE) {
                    this.setStatus('Вы проиграли <a class="js-new-game" href="#" title="Новая игра (F2)"><span class="glyphicon glyphicon-refresh"></span></a>');
                }
                // выигрыш
                else if (status === gameStatus.WIN) {
                    this.setStatus('Победа! <a class="js-new-game" href="#" title="Новая игра (F2)"><span class="glyphicon glyphicon-refresh"></span></a>');
                }
            })
            .on('update', this.paintCell.bind(this))
            .on('gameStarted', () => {
                this.startTimer();
            })
            .on('flags', (flags) => {
                this.flagCounter.innerHTML = flags;
            })
            .on('open', () => {
                this.setStatus('Открыто ячеек: ' + this.game.getOpenedCount() + ' / ' + this.game.getNeedOpenCount());
            })
        ;
    }

    setStatus(message) {
        this.statusBar.innerHTML = message;
    }

    // перерисовывает все ячейки
    paintAll() {
        this.canvas.width = this.game.nHor * w + 2;
        this.canvas.height = this.game.nVert * h + 2;

        for (let col = 1; col <= this.game.nHor; col++) {
            for (let row = 1; row <= this.game.nVert; row++) {
                this.paintCell(row, col);
            }
        }
    }

    // перерисовывает одну ячейку
    paintCell(r, c) {
        const ctx = this.ctx;
        const x = (c - 1) * w + 1;
        const y = (r - 1) * h + 1;
        const cells = this.game.cells;

        if (cells.isOpen(r, c)) {
            ctx.fillStyle = '#cccccc';
            ctx.fillRect(x, y, w, h);

            ctx.lineWidth = 1;
            ctx.strokeStyle = '#808080';
            ctx.strokeRect(x + 0.5, y + 0.5, w - 2, h - 2);
            if (cells.isMine(r, c)) {
                if (cells.isFail(r, c)) {
                    ctx.fillStyle = '#cd5c5c';
                    ctx.fillRect(x + 1, y + 1, w - 3, h - 3);
                }
                ctx.drawImage(mine, x + 1, y);
            } else {
                // цифры
                ctx.font = 'bold 18px/1 Tahoma, Geneva, Verdana';
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';

                 let numberX = x + Math.floor(w / 2);
                 let numberY = y + Math.floor(h / 2);

                let number = cells.getNumber(r, c);
                if (number > 0) {
                    ctx.fillStyle = '#' + colors[number];
                    ctx.fillText(number, numberX, numberY);
                }
            }
        }
        // ячейка закрыта
        else {
            ctx.fillStyle = '#d3d3d3';
            ctx.fillRect(x, y, w, h);

            ctx.lineWidth = 2;
            ctx.beginPath();
            ctx.moveTo(x + 1, y + h - 2);
            ctx.lineTo(x + 1, y + 1);
            ctx.lineTo(x + w - 2, y + 1);
            ctx.strokeStyle = '#ffffff';
            ctx.stroke();
            ctx.closePath();
            ctx.beginPath();
            ctx.moveTo(x + 1, y + h - 2);
            ctx.lineTo(x + w - 2, y + h - 2);
            ctx.lineTo(x + w - 2, y + 1);
            ctx.strokeStyle = '#808080';
            ctx.stroke();
            ctx.closePath();

            if (cells.isFlag(r, c)) {
                ctx.drawImage(flag, x, y);
            } else if (cells.isQuestion(r, c)) {
                ctx.drawImage(question, x + 1, y + 2);
            }
        }
    }

    startTimer() {
        utils.log('view: start timer');
        this.stopTimer();
        this.timer = setInterval(() => {
            this.updateTimer();
        }, 500);
    }

    updateTimer() {
        this.setTimer(this.game.getResult());
    }

    setTimer(value) {
        this.timeCounter.innerHTML = resultFormatter(value, false);
    }

    stopTimer() {
        utils.log('view: stop timer');
        clearInterval(this.timer);
    }

    get cellWidth() {
        return w;
    }

    get cellHeight() {
        return h;
    }
}

module.exports = View;
