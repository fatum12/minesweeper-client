const $ = require('jquery');

const Game = require('./game');
const sound = require('../sound');
const socket = require('../socket');
const View = require('./view');
const events = require('../events');
const gameType = require('../constants/gameType');
const gameStatus = require('../constants/gameStatus');
const strategyGenerator = require('./strategyGenerator');
const localStrategy = require('./strategies/local');

const LEFT_MOUSE_BUTTON = 1;
const MIDDLE_MOUSE_BUTTON = 2;
const RIGHT_MOUSE_BUTTON = 3;

const game = new Game();
let canvas;
let flagCounter;
let timeCounter;
let statusBar;
let view;
let initialized = false;

function init() {
    canvas = document.getElementById('game-area');
    flagCounter = document.getElementById('flags-count');
    timeCounter = document.getElementById('time-count');
    statusBar = document.getElementById('status');

    view = new View(game, canvas, flagCounter, timeCounter, statusBar);

    canvas.addEventListener('mouseup', (e) => {
        if (e.which == LEFT_MOUSE_BUTTON) {
            let {row, col} = getIndexes(e);
            game.open(row, col);
        } else if (e.which == MIDDLE_MOUSE_BUTTON) {
            let {row, col} = getIndexes(e);
            game.openAround(row, col);
        }
    });
    canvas.addEventListener('mousedown', (e) => {
        e.preventDefault();
        if (e.which == RIGHT_MOUSE_BUTTON) {
            let {row, col} = getIndexes(e);
            game.mark(row, col);
        }
    });
    canvas.addEventListener('dblclick', (e) => {
        if (e.which == LEFT_MOUSE_BUTTON) {
            let {row, col} = getIndexes(e);
            game.openAround(row, col);
        }
    });

    socket.on(events.GAME_CREATE_RESULT, (data) => {
        game.sync(data.cells);
    });
    socket.on(events.DISCONNECT, () => {
        game.strategy = localStrategy;
    });

    game
        .on('init', (type, nHor, nVert, nMines) => {
            activateMenuItem(type);
            if (type == gameType.INTERMEDIATE) {
                location.hash = 'intermediate';
            } else if (type == gameType.EXPERT) {
                location.hash = 'expert';
            } else {
                location.hash = '';
            }
        })
        .on('endGame', (status) => {
            // проигрыш
            if (status == gameStatus.LOSE) {
                sound.play('lose');
            }
            // выигрыш
            else if (status == gameStatus.WIN) {
                sound.play('win');
            }
        })
        .on('gameStarted', () => {
            sound.play('game_start');
        })
    ;

    socket.once(events.CONNECT, () => {
        run();
    });

    setTimeout(() => {
        run();
    }, 6000);
}

function run() {
    if (initialized) {
        return;
    }
    initialized = true;

    $(document.body).on('click', '.js-new-game', function (e) {
        e.preventDefault();

        newGameByGameType($(this).data('game-type') ? $(this).data('game-type') : game.type);
    });

    $(document).keydown((e) => {
        // начинаем новую игру при нажатии F2
        if (e.which == 113) {
            e.preventDefault();
            newGameByGameType(game.type);
        }
    });

    switch (location.hash) {
        case '#intermediate':
            newGameByGameType(gameType.INTERMEDIATE);
            break;
        case '#expert':
            newGameByGameType(gameType.EXPERT);
            break;
        default:
            newGameByGameType(gameType.BEGINNER);
    }

    $('#loader').remove();
}

function activateMenuItem(type) {
    $('.game-type-item')
        .removeClass('active')
        .find(`[data-game-type=${type}]`).parent().addClass('active');
}

function getIndexes(e) {
    let box = canvas.getBoundingClientRect();

    return {
        col: Math.floor((e.pageX - (box.left + pageXOffset) - 1) / view.cellWidth) + 1,
        row: Math.floor((e.pageY - (box.top + pageYOffset) - 1) / view.cellHeight) + 1
    };
}

function newGame(hor, vert, mines) {
    const type = Game.determineGameType(hor, vert, mines);
    game.strategy = strategyGenerator(type);
    game.newGame(hor, vert, mines);
    game.createMines();
}

function newGameByGameType(type) {
    game.strategy = strategyGenerator(type);
    game.newGameByGameType(type);
    game.createMines();
}

module.exports = {
    init: init,
    getGame: () => { return game; },
    newGame: newGame,
    newGameByGameType: newGameByGameType
};
