// свойства ячейки хранятся в битах числа
const MASK_MINE = 0x10;
const MASK_OPEN = 0x20;
const MASK_FLAG = 0x40;
const MASK_QUESTION = 0x80;
const MASK_FAIL = 0x100;
const MASK_NUMBER = 0xF;

module.exports = class Field {

    constructor() {
        this.cells = [];
    }

    setSize(rows, cols) {
        this.cells = [];
        for (let row = 0; row <= rows + 1; row++) {
            this.cells.push([]);
            for (let col = 0; col <= cols + 1; col++) {
                this.cells[row].push(0);
            }
        }
    }

    incrementNumber(r, c) {
        this.cells[r][c]++;

        return this;
    }

    decrementNumber(r, c) {
        this.cells[r][c]--;

        return this;
    }

    getNumber(r, c) {
        return this.cells[r][c] & MASK_NUMBER;
    }

    isMine(r, c) {
        return this.isBitSet(r, c, MASK_MINE);
    }

    makeMine(r, c) {
        this.setBit(r, c, MASK_MINE);
        // пересчет количества мин вокруг ячейки
        for (let row = r - 1; row <= r + 1; row++) {
            for (let col = c - 1; col <= c + 1; col++) {
                if (row === r && col === c) {
                    continue;
                }
                this.incrementNumber(row, col);
            }
        }

        return this;
    }

    resetMine(r, c) {
        this.resetBit(r, c, MASK_MINE);
        // пересчет количества мин вокруг ячейки
        for (let row = r - 1; row <= r + 1; row++) {
            for (let col = c - 1; col <= c + 1; col++) {
                if (row === r && col === c) {
                    continue;
                }
                this.decrementNumber(row, col);
            }
        }

        return this;
    }

    isOpen(r, c) {
        return this.isBitSet(r, c, MASK_OPEN);
    }

    makeOpen(r, c) {
        this.setBit(r, c, MASK_OPEN);

        return this;
    }

    close(r, c) {
        this.resetBit(r, c, MASK_OPEN);

        return this;
    }

    isFlag(r, c) {
        return this.isBitSet(r, c, MASK_FLAG);
    }

    makeFlag(r, c) {
        this.setBit(r, c, MASK_FLAG);

        return this;
    }

    resetFlag(r, c) {
        this.resetBit(r, c, MASK_FLAG);

        return this;
    }

    isQuestion(r, c) {
        return this.isBitSet(r, c, MASK_QUESTION);
    }

    makeQuestion(r, c) {
        this.setBit(r, c, MASK_QUESTION);

        return this;
    }

    resetQuestion(r, c) {
        this.resetBit(r, c, MASK_QUESTION);

        return this;
    }

    isFail(r, c) {
        return this.isBitSet(r, c, MASK_FAIL);
    }

    makeFail(r, c) {
        this.setBit(r, c, MASK_FAIL);

        return this;
    }

    isBitSet(r, c, mask) {
        return (this.cells[r][c] & mask) !== 0;
    }

    setBit(r, c, mask) {
        this.cells[r][c] |= mask;
    }

    resetBit(r, c, mask) {
        this.cells[r][c] &= ~mask;
    }

    toJSON() {
        return this.cells;
    }
}
