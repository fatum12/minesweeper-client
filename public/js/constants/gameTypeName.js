const gameType = require('./gameType');

module.exports = Object.freeze({
    [gameType.BEGINNER]: 'новичок',
    [gameType.INTERMEDIATE]: 'любитель',
    [gameType.EXPERT]: 'профессионал'
});