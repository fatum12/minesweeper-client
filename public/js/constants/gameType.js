module.exports = Object.freeze({
    BEGINNER: 1,
    INTERMEDIATE: 2,
    EXPERT: 3,
    CUSTOM: 4
});