const $ = require('jquery');
const howler = require('howler');

const storage = require('./storage');

let muted = true;
let sounds = {};
let $btnSound;

function init() {
    $.each(['game_start', 'lose', 'message', 'win'], (index, value) => {
        sounds[value] = new howler.Howl({
            src: [`sounds/${value}.mp3`, `sounds/${value}.ogg`],
            autoplay: false,
            loop: false
        });
    });

    $btnSound = $('#btn-sound');
    $btnSound.on('click', (e) => {
        e.preventDefault();
        toggle();
    });

    if (storage.get('sound', 0) == 1) {
        enable();
    }
}

function isMuted() {
    return muted;
}

function play(id) {
    if (isMuted()) {
        return;
    }
    if (id in sounds) {
        sounds[id].play();
    } else {
        throw new Error(`Sound "${id}" not found`);
    }
}

function enable() {
    muted = false;
    $btnSound.attr('title', 'Выключить звук')
        .find('.glyphicon').removeClass('glyphicon-volume-off').addClass('glyphicon-volume-up');

    storage.set('sound', 1);
}

function disable() {
    muted = true;
    $btnSound.attr('title', 'Включить звук')
        .find('.glyphicon').removeClass('glyphicon-volume-up').addClass('glyphicon-volume-off');

    storage.set('sound', 0);
}

function toggle() {
    if (isMuted()) {
        enable();
    } else {
        disable();
    }
}

module.exports = {
    init: init,
    play: play
};