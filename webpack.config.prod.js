const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const dev = require('./webpack.config');

dev.plugins.push(new UglifyJsPlugin());

module.exports = dev;
