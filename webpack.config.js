const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: './public/js/app.js',
    output: {
        filename: '[name].js',
        publicPath: '/build/',
        path: path.resolve(__dirname, 'public', 'build')
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        { loader: 'css-loader', options: { minimize: true } }
                    ]
                })
            },
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: [['es2015', {modules: false}]]
                    }
                }]
            },
            {
                test: /\.(png|jpg|gif|ttf|eot|woff2?|svg)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        publicPath: 'files/',
                        outputPath: 'files/'
                    }
                }]
            },
            {
                test: /\.handlebars$/,
                use: [{
                    loader: 'handlebars-loader',
                    options: {
                        helperDirs: path.resolve(__dirname, 'public', 'js', 'helpers')
                    }
                }]
            }
        ]
    },
    plugins: [
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru/),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: function (module) {
                // this assumes your vendor imports exist in the node_modules directory
                return module.context && module.context.indexOf('node_modules') !== -1;
            }
        }),
        new ExtractTextPlugin('[name].css'),
        new webpack.ProvidePlugin({
            jQuery: 'jquery',
            $: 'jquery'
        })
    ],
    devServer: {
        contentBase: path.join(__dirname, 'public'),
        port: 9000,
        proxy: {
            '/api': 'http://localhost:9999',
            '/minesweeper': {
                target: 'http://localhost:9999',
                ws: true
            }
        }
    }
};
